using System.Text.RegularExpressions;

namespace Rayark.PatchwerkLite
{
    public static class StringUtility
    {
        public static string InsertAllAfter(string target, string searchTarget, string insertString)
            => target.Replace(searchTarget, searchTarget + insertString);

        public static string InsertAllAfterWithNewLine(string target, string searchTarget, string insertString)
            => InsertAllAfter(target, searchTarget, "\n" + insertString);

        public static string InsertAfterMatch(string target, string regex, string insertString)
        {
            var match = Regex.Match(target, regex);
            return match.Success ? target.Insert(match.Index + match.Length, insertString)
                                 : null;
        }
    }
}