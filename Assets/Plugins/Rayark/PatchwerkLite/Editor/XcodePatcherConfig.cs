using System;
using System.IO;
using System.Collections.Generic;

using UnityEngine;

namespace Rayark.PatchwerkLite
{
    public enum UsageDescriptionLocale
    {
        English,
        Chinese,
        ChineseTraditional,
        ChineseSimplified,
        Japanese,
        Korean,
        Vietnamese,
        Thai,
        Portuguese,
        Spanish,
        German,
        Russian,
        French,
        Arabic
    }

    /// <summary>
    /// Xcode patching config
    /// </summary>
    public class XcodePatcherConfig
    {
        // Device Capability
        internal bool IsAddingExtraDeviceCapabilities;
        internal string[] ExtraDeviceCapabilities;

        /// <summary>
        /// Add extra device capability requirement to Info.plist (UIRequiredDeviceCapabilities)
        /// Capabilities list:
        /// https://developer.apple.com/support/required-device-capabilities/
        /// It will always include basic capability `armv7`
        /// You do not need to include `armv7`
        /// </summary>
        public void AddExtraDeviceCapabilities(string[] capabilities)
        {
            IsAddingExtraDeviceCapabilities = true;
            ExtraDeviceCapabilities = capabilities;
        }


        // Framework
        internal bool IsAddingExtraFramework;
        internal string[] ExtraFrameworkNames;
        internal string[] ExtraOptionalFrameworkNames;

        /// <summary>
        /// Add extra framework reference (required/optional) to PBX project
        /// TwitterKit requires `CoreData.framework`
        /// Sign in with Apple requires `AuthenticationServices.framework`
        /// Facebook later than 8.0.0 requires `MobileCoreServices.framework`
        /// </summary>
        public void AddExtraFrameworks(string[] frameworkNames, string[] optionalFrameworkNames)
        {
            IsAddingExtraFramework = true;
            ExtraFrameworkNames = frameworkNames;
            ExtraOptionalFrameworkNames = optionalFrameworkNames;
        }


        /// <summary>
        /// Is adding `com.apple.iCloud` to SystemCapabilities under TargetAttributes in PBX project
        /// You need to set up entitlement file as well
        /// </summary>
        public bool IsAddingiCloudEntitlement { get; set; } = false;

        /// <summary>
        /// Is adding `com.apple.BackgroundModes` and `com.apple.Push` to SystemCapabilities in PBX project
        /// Also add `remote-notification` to `UIBackgroundModes` in Info.plist
        /// You need to set up entitlement file as well
        /// </summary>
        public bool IsAddingPushNotificationEntitlement { get; set; } = false;

        // System Capabilities
        internal string DevelopmentTeamId = string.Empty;

        /// <summary>
        /// If you set anything related to SystemCapabilities, such as iCloud and push notification
        /// You need to supply ID of DevelopmentTeam
        /// It's 10 characters string which can be found on distribution certificate in Keychain
        /// https://stackoverflow.com/a/47732584
        /// </summary>
        public void SetSystemCapabilitiesDevelopmentTeamId
        (
            string developmentTeamId
        ) => DevelopmentTeamId = developmentTeamId;

        // Entitlements File Config
        internal bool IsCopyingEntitlementFileRequired
            => IsAddingiCloudEntitlement || IsAddingPushNotificationEntitlement;

        internal string EntitlementSourceFileAssetPath = string.Empty;

        /// <summary>
        /// If you set anything requires entitlement file, such as iCloud, push notification or Sign in with Apple
        /// You need to supply entitlement file
        /// You can get the entitlement file by turning on entitlements in Xcode GUI
        /// Copy `Xcode folder/Unity-iPhone/bundle name.entitlements` back to your Unity repo
        /// </summary>
        public void SetEntitlement(string sourceFilePath)
        {
            EntitlementSourceFileAssetPath = sourceFilePath;
        }

        internal string EntitlementFileName
        {
            get
            {
                try
                {
                    return Path.GetFileName(EntitlementSourceFileAssetPath);
                }
                catch (ArgumentException e)
                {
                    Debug.LogError(e);
                    return string.Empty;
                }
            }
        }

        public bool HasEntitlement
        {
            get => !string.IsNullOrEmpty(EntitlementSourceFileAssetPath);
        }


        /// <summary>
        /// Turn off Bitcode by setting `ENABLE_BITCODE` to `NO` in PBX project BuildProperty
        /// You need to turn off Bitcode if any of your iOS plugins does not support Bitcode
        /// </summary>
        public bool IsDisablingBitcode { get; set; } = true;

        /// <summary>
        /// Turn on Objective-C exception handling by setting `GCC_ENABLE_OBJC_EXCEPTIONS` to `YES` in PBX project BuildProperty
        /// You need to turn on Objective-C exception if any of your iOS plugins throws Objective-C exception
        /// </summary>
        public bool IsEnablingObjcExceptions { get; set; } = false;

        /// <summary>
        /// Embed standard Swift libraries by setting `ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES` to `YES` in PBX project BuildProperty
        /// You need to embed Swift libraries if you have Swift dependencies and need to support iOS 11 and older OS
        /// This is required to run Facebook SDK 7.21.2 and later on iOS 11 and earlier
        /// </summary>
        public bool IsEmbeddingSwiftStandardLibraries { get; set; } = false;

        // Facebook App Transport Security
        internal bool IsAddingFacebookAppTransportSecurity;
        internal bool IsWhiteListingFacebookSdkOlderThanV45;
        internal bool IsWhiteListingFacebookSdkOlderThanV46;
        internal bool IsWhiteListingFacebookSdkV46;

        /// <summary>
        /// Add AppTransportSecurity for Facebook SDK
        /// See: https://developers.facebook.com/docs/ios/ios9/
        /// </summary>
        public void SetFacebookAppTransportSecurity
        (
            bool isWhiteListingPreV45,
            bool isWhiteListingPreV46,
            bool isWhiteListingV46
        )
        {
            IsAddingFacebookAppTransportSecurity = true;
            IsWhiteListingFacebookSdkOlderThanV45 = isWhiteListingPreV45;
            IsWhiteListingFacebookSdkOlderThanV46 = isWhiteListingPreV46;
            IsWhiteListingFacebookSdkV46 = isWhiteListingV46;
        }

        /// <summary>
        /// Set BundleIdentifier to App ID
        /// You need to set BundleIdentifier if you want to use fastlane on Xcode project
        /// </summary>
        public bool IsSettingProductBundleIdentifier { get; set; } = false;

        internal bool IsCopyingFastlaneFolder;
        internal string FastlaneFolderPath = string.Empty;

        /// <summary>
        /// Copy fastlane settings folder to Xcode project
        /// </summary>
        public void SetFastlaneFolderPath(string path)
        {
            IsCopyingFastlaneFolder = true;
            FastlaneFolderPath = path;
        }

        // CFBundleURL Settings
        internal bool IsAddingCFBundleURL;
        internal string CFBundleURLName;
        internal string[] CFBundleURLSchemes;

        /// <summary>
        /// Set URL scheme
        /// </summary>
        public void SetCFBundleURL(string cfBundleURLName, string[] cfBundleSchemes)
        {
            IsAddingCFBundleURL = true;
            CFBundleURLName = cfBundleURLName;
            CFBundleURLSchemes = cfBundleSchemes;
        }

        // CFBundleLocalizations
        internal bool IsAddingCFBundleLocalizations;
        internal string[] CFBundleLocalizations;

        /// <summary>
        /// Declare supported localization
        /// See: https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundlelocalizations
        /// </summary>
        public void SetCFBundleLocalizations(string[] cfBundleLocalizations)
        {
            IsAddingCFBundleLocalizations = true;
            CFBundleLocalizations = cfBundleLocalizations;
        }

        internal bool IsAddingPhotoLibraryUsage;
        internal string PhotoLibraryUsageDescription = string.Empty;
        internal Dictionary<UsageDescriptionLocale, string> PhotoLibraryUsageDescriptionLocalization;

        /// <summary>
        /// Set string to `NSPhotoLibraryUsageDescription` and `NSPhotoLibraryAddUsageDescription`
        /// This string will be displayed when you try to access photo library for user like saving screenshots
        /// If you access photo library without setting this string, it will trigger
        /// `ITMS-90683: Missing Purpose String in Info.plist` warning after uploading a build
        /// You can supply localizations for different languages
        /// </summary>
        public void SetPhotoLibraryUsageDescription
        (
            string defaultDescription,
            Dictionary<UsageDescriptionLocale, string> descriptionLocalization = null
        )
        {
            IsAddingPhotoLibraryUsage = true;
            PhotoLibraryUsageDescription = defaultDescription;
            PhotoLibraryUsageDescriptionLocalization = descriptionLocalization;
        }

        internal bool IsAddingLocationWhenInUseUsageDescription;
        internal string LocationWhenInUseUsageDescription = string.Empty;
        internal Dictionary<UsageDescriptionLocale, string> LocationWhenInUseUsageDescriptionLocalization;

        /// <summary>
        /// Set string to `NSLocationWhenInUseUsageDescription`
        /// This string will be displayed when you try to access user's location
        /// If you access user's location without setting this string, it will trigger
        /// `ITMS-90683: Missing Purpose String in Info.plist` warning after uploading a build
        /// You can supply localizations for different languages
        /// </summary>
        public void SetAddingLocationWhenInUseUsageDescription
        (
            string description,
            Dictionary<UsageDescriptionLocale, string> descriptionLocalization = null
        )
        {
            IsAddingLocationWhenInUseUsageDescription = true;
            LocationWhenInUseUsageDescription = description;
            LocationWhenInUseUsageDescriptionLocalization = descriptionLocalization;
        }

        internal bool IsAddingUserTrackingUsageDescription;
        internal string UserTrackingUsageDescription = string.Empty;
        internal Dictionary<UsageDescriptionLocale, string> UserTrackingUsageDescriptionLocalization;

        /// <summary>
        /// Set string to `NSUserTrackingUsageDescription`
        /// This string will be displayed when you try to prompt tracking option with App Tracking Transparency framework
        /// You can supply localizations for different languages
        /// </summary>
        public void SetAddingUserTrackingUsageDescription
        (
            string description,
            Dictionary<UsageDescriptionLocale, string> descriptionLocalization = null
        )
        {
            IsAddingUserTrackingUsageDescription = true;
            UserTrackingUsageDescription = description;
            UserTrackingUsageDescriptionLocalization = descriptionLocalization;
        }

        /// <summary>
        /// Set `CFBundleAllowMixedLocalizations` in Info.plist
        /// If any UIKit popup shows in wrong localization, turn this option on
        /// </summary>
        public bool IsAllowMixedLocalizations { get; set; } = false;

        /// <summary>
        /// Set `ITSAppUsesNonExemptEncryption` in Info.plist
        /// You don't need to answer encryption question on iTunes Connect if you added this field
        /// </summary>
        public bool IsUsingNonExemptEncryption { get; set; } = false;

        /// <summary>
        /// Set Runtime Sanitization mode
        /// </summary>
        public bool IsRunWithAddressSanitizer { get; set; } = false;

        internal bool IsSettingCFBundleName;
        internal string CFBundleName;

        /// <summary>
        /// Set `CFBundleName` in Info.plist
        /// This bundle name will show up when you try to login with 3rd party service
        /// </summary>
        public void SetCFBundleName(string bundleName)
        {
            IsSettingCFBundleName = true;
            CFBundleName = bundleName;
        }

        // Replace Files
        internal List<ReplaceFilePathData> ReplaceFilePaths = new List<ReplaceFilePathData>();

        /// <summary>
        /// Set files to replace in built Xcode project
        /// </summary>
        public void AddReplaceFilePath(string sourceAbsolutePath, string destinationRelativePath)
            => ReplaceFilePaths.Add(
                new ReplaceFilePathData
                {
                    SourceAbsolutePath = sourceAbsolutePath,
                    DestinationRelativePath = destinationRelativePath
                });

        // Add Plist Reference Files
        internal List<AddFileReferenceData> AddToPbxProjectPlistFiles = new List<AddFileReferenceData>();

        /// <summary>
        /// Set Plist files to copy and add reference in built Xcode project
        /// </summary>
        public void AddPlistFileToPbxProject
        (
            string sourceFilePath,
            string destinationFilename
        )
            => AddToPbxProjectPlistFiles.Add(
                new AddFileReferenceData
                {
                    SourceFilePath = sourceFilePath,
                    DestinationFilename = destinationFilename
                });

        // Add Swift Reference Files
        internal List<AddFileReferenceData> AddToPbxProjectSwiftFiles = new List<AddFileReferenceData>();

        /// <summary>
        /// Set Swift files to copy and add reference in built Xcode project
        /// </summary>
        public void AddSwiftFileToPbxProject
        (
            string sourceFilePath,
            string destinationFilename
        )
            => AddToPbxProjectSwiftFiles.Add(
                new AddFileReferenceData
                {
                    SourceFilePath = sourceFilePath,
                    DestinationFilename = destinationFilename
                });


        internal bool IsAddingSwiftBridgingHeader;
        internal string SwiftBridgingHeaderSourceFilePath;
        internal string SwiftBridgingHeaderDestinationFilename;

        /// <summary>
        /// Add and Setup Swift bridging C header file
        /// </summary>
        public void AddSwiftBridgingHeader
        (
            string sourceFilePath,
            string destinationFilename
        )
        {
            IsAddingSwiftBridgingHeader = true;

            SwiftBridgingHeaderSourceFilePath = sourceFilePath;
            SwiftBridgingHeaderDestinationFilename = destinationFilename;
        }

        internal Dictionary<string, string> XCBuildConfigurationAttributes =
             new Dictionary<string, string>();

        /// <summary>
        /// Set Xcode Build Configuration Attributes
        /// </summary>
        public void SetXcodeBuildConfiguration(string attributeName, string value)
            => XCBuildConfigurationAttributes.Add(attributeName, value);
    }

    public class ReplaceFilePathData
    {
        public string SourceAbsolutePath;
        public string DestinationRelativePath;
    }

    public class AddFileReferenceData
    {
        public string SourceFilePath;
        public string DestinationFilename;
    }
}