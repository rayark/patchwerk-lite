#if UNITY_IOS
using System;
using System.IO;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

using UnityEngine;
using UnityEditor;
using UnityEditor.iOS.Xcode;
using UnityEditorInternal;

namespace Rayark.PatchwerkLite
{
    public class XcodePatcher
    {
        private static readonly string[] defaultCapabilityStrings = { "armv7" };

        private const string _pbxFileReferenceAnchor =
            @"/* Begin PBXFileReference section */";

        private const string _entitlementPbxFileReferenceTemplate =
            @"{0} /* {1} */ = {{isa = PBXFileReference; lastKnownFileType = text.plist.entitlements; name = {1}; path = ""Unity-iPhone/{1}""; sourceTree = ""<group>""; }};";
        private const string _plistPbxFileReferenceTemplate =
            @"{0} /* {1} */ = {{isa = PBXFileReference; fileEncoding = 4; lastKnownFileType = text.plist.xml; path = ""{1}""; sourceTree = ""<group>""; }};";
        private const string _textPbxFileReferenceTemplate =
            @"{0} /* {1} */ = {{isa = PBXFileReference; lastKnownFileType = text.plist.strings; name = ""{1}"" path = ""{2}""; sourceTree = ""<group>""; }};";
        private const string _swiftPbxFileReferenceTemplate =
            @"{0} /* {1} */ = {{isa = PBXFileReference; fileEncoding = 4; lastKnownFileType = sourcecode.swift; path = ""{1}""; sourceTree = ""<group>""; }};";
        private const string _cHeaderPbxFileReferenceTemplate =
            @"{0} /* {1} */ = {{isa = PBXFileReference; lastKnownFileType = sourcecode.c.h; path = ""{1}""; sourceTree = ""<group>""; }};";
            
        private const string _pbxBuildFileAnchor =
            @"/* Begin PBXBuildFile section */";
        private const string _pbxBuildFileReferenceTemplate =
            @"{0} /* {1} in Resources */ = {{isa = PBXBuildFile; fileRef = {2} /* {1} */; }};";

        private const string _customTemplateAnchor =
            "/* CustomTemplate */ = {\n\t\t\tisa = PBXGroup;\n\t\t\tchildren = (";
        private const string _customTemplateContentTemplate =
            @"{0} /* {1} */,";

        private const string _pbxVariantGroupAnchor =
            @"/* Begin PBXVariantGroup section */";
        private const string _pbxVariantGroupTemplate =
            @"{0} /* {1} */ = {{
			isa = PBXVariantGroup;
			children = (
				{2}
			);
			name = {1};
			sourceTree = ""<group>"";
        }};";

        private const string _pbxResourcesBuildPhaseRegex =
            @"\/\* Begin PBXResourcesBuildPhase section \*\/\n\t*[A-F0-9]{24} \/\* Resources \*\/ = {\n\t*isa = PBXResourcesBuildPhase;\n\t*buildActionMask = [0-9]*;\n\t*files = \(\n";
        private const string _pbxResourcesBuildPhaseTemplate =
            "{0} /* {1} in Resources */,";

        private const string _targetAttributeAnchor =
            "TargetAttributes = {";

        private const string _developmentTeamTemplate =
            "\t\t\t\t\t\tDevelopmentTeam = {0};";

        private const string _systemCapabilitiesAnchor =
            "SystemCapabilities = {";

        private const string _targetAttributeContentTemplate =
            @"		{0} = {{
						DevelopmentTeam = {1};
						SystemCapabilities = {{
							{2}
						}};
					}};";

        private const string _systemCapabilityTemplate =
            @"              {0} = {{
                                enabled = 1;
                            }};
            ";

        private const string _entitlementContentTemplate =
            @"CODE_SIGN_ENTITLEMENTS = ""Unity-iPhone/{0}"";";

        private const string _xcBuildConfigurationAnchor =
            "isa = XCBuildConfiguration;\n\t\t\tbuildSettings = {";

        private const string _gccEnableObjcException = "GCC_ENABLE_OBJC_EXCEPTIONS";
        private const string _enableBitcode = "ENABLE_BITCODE";
        private const string _embeddingSwiftStandardLibraries = "ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES";
        private const string _entitlementDestinationRelativePathTemplate = "./Unity-iPhone/{0}";
        private const string _productBundleIdentifierAnchor = @"INFOPLIST_FILE = Info.plist;";

        private const string _productBundleIdentifierTemplate =
            "INFOPLIST_FILE = Info.plist;\nPRODUCT_BUNDLE_IDENTIFIER = {0};";

        private const string _pbxSourcesBuildPhaseRegex =
            @"\/\* Begin PBXSourcesBuildPhase section \*\/\n\t*[A-F0-9]{24} \/\* Sources \*\/ = {\n\t*isa = PBXSourcesBuildPhase;\n\t*buildActionMask = [0-9]*;\n\t*files = \(\n";
        private const string _pbxSourcesBuildPhaseTemplate =
            "{0} /* {1} in Sources */,";

        private const string _xcBuildConfigurationAttributeRegexTemplate =
            "{0} = \\\"[^\\\"]*\\\";";
        private const string _xcBuildConfigurationAttributeTemplate =
            "{0} = \"{1}\";";

        


        public static void Patch
        (
            string pathToBuiltProject,
            XcodePatcherConfig config
        )
        {
            _SetDeviceCapabilities(pathToBuiltProject,
                config.IsAddingExtraDeviceCapabilities,
                config.ExtraDeviceCapabilities);

            _AddFrameWork(pathToBuiltProject,
                config.IsAddingExtraFramework,
                config.ExtraFrameworkNames,
                config.ExtraOptionalFrameworkNames);

            _SetBuildSettings(pathToBuiltProject, 
                _PrepareBuildSettings(config));

            {
                var newSystemCapabilities = new List<string>();

                _SetiCloudEntitlement(config.IsAddingiCloudEntitlement, newSystemCapabilities);

                _SetPushNotification(pathToBuiltProject,
                    config.IsAddingPushNotificationEntitlement, newSystemCapabilities);

                _WriteSystemCapabilities(pathToBuiltProject,
                    config.DevelopmentTeamId,
                    newSystemCapabilities);
            }

            _SetToFullScreenOnly(pathToBuiltProject);

            _SetFacebookAppTransportSecurityWhiteList(pathToBuiltProject,
                config.IsAddingFacebookAppTransportSecurity,
                config.IsWhiteListingFacebookSdkOlderThanV45,
                config.IsWhiteListingFacebookSdkOlderThanV46,
                config.IsWhiteListingFacebookSdkV46);

            _SetProductBundleIdentifier(pathToBuiltProject,
                config.IsSettingProductBundleIdentifier,
                PlayerSettings.applicationIdentifier);

            _CopyFastlaneFolder(pathToBuiltProject,
                config.IsCopyingFastlaneFolder,
                config.FastlaneFolderPath);

            _AddBundleURL(pathToBuiltProject,
                config.IsAddingCFBundleURL,
                config.CFBundleURLName,
                config.CFBundleURLSchemes);

            _AddBundleLocalizations(pathToBuiltProject, 
                config.CFBundleLocalizations);



            Dictionary<UsageDescriptionLocale, List<string>> usageDescriptionLocales =
                new Dictionary<UsageDescriptionLocale, List<string>>();

            _SetAddingPhotoLibraryDescription(pathToBuiltProject,
                config.IsAddingPhotoLibraryUsage,
                config.PhotoLibraryUsageDescription,
                config.PhotoLibraryUsageDescriptionLocalization,
                usageDescriptionLocales);

            _SetLocationWhenInUseUsageDescription(pathToBuiltProject,
                config.IsAddingLocationWhenInUseUsageDescription,
                config.LocationWhenInUseUsageDescription,
                config.LocationWhenInUseUsageDescriptionLocalization,
                usageDescriptionLocales);

            _SetUserTrackingUsageDescription(pathToBuiltProject,
                config.IsAddingUserTrackingUsageDescription,
                config.UserTrackingUsageDescription,
                config.UserTrackingUsageDescriptionLocalization,
                usageDescriptionLocales);

            _SetUsageDescriptionLocalization(pathToBuiltProject,
                usageDescriptionLocales);



            _SetIsAllowMixedLocalizations(pathToBuiltProject,
                config.IsAllowMixedLocalizations);

            _SetAppUsesNonExemptEncryption(pathToBuiltProject,
                config.IsUsingNonExemptEncryption);


            _ReplaceFiles(pathToBuiltProject,
                config.ReplaceFilePaths);


            _InstallEntitlementsFile(pathToBuiltProject,
                config.HasEntitlement,
                config.IsCopyingEntitlementFileRequired,
                config.EntitlementFileName,
                config.EntitlementSourceFileAssetPath);

            _SetRunWithAddressSanitizer(pathToBuiltProject,
                config.IsRunWithAddressSanitizer);

            _SetBundleName(pathToBuiltProject,
                config.IsSettingCFBundleName,
                config.CFBundleName);

            _AddPlistFileToReferences(pathToBuiltProject,
                config.AddToPbxProjectPlistFiles);

            _AddSwiftFileToReferences(pathToBuiltProject,
                config.AddToPbxProjectSwiftFiles);

            _AddSwiftBridgingHeader(pathToBuiltProject,
                config.IsAddingSwiftBridgingHeader,
                config.SwiftBridgingHeaderSourceFilePath,
                config.SwiftBridgingHeaderDestinationFilename);

            _SetXcodeBuildConfiguration(pathToBuiltProject,
                config.XCBuildConfigurationAttributes);
        }


        private static void _SetDeviceCapabilities
        (
            string pathToBuiltProject,
            bool isAddingExtraDeviceCapabilities,
            string[] extraCapabilityString
        )
        {
            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            var deviceCapabilityArray = infoPlistRootDict.CreateArray("UIRequiredDeviceCapabilities");
            foreach (var capabilityString in defaultCapabilityStrings)
            {
                deviceCapabilityArray.AddString(capabilityString);
            }

            if (isAddingExtraDeviceCapabilities && extraCapabilityString != null)
            {
                foreach (var capabilityString in extraCapabilityString)
                {
                    deviceCapabilityArray.AddString(capabilityString);
                }
            }

            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }


        private static void _AddFrameWork
        (
            string pathToBuiltProject,
            bool isAddingExtraFramework,
            string[] extraFrameworkNames,
            string[] extraOptionalFrameworkNames
        )
        {
            if (!isAddingExtraFramework)
            {
                return;
            }

            var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxProjectPath);

            var targetGuid = _GetMainTargetGuid(pbxProject);
            _AddFrameWorkToProject(pbxProject, targetGuid, extraFrameworkNames, extraOptionalFrameworkNames);

#if UNITY_2019_3_OR_NEWER
            var frameworkGuid = _GetUnityFrameworkGuid(pbxProject);
            _AddFrameWorkToProject(pbxProject, frameworkGuid, extraFrameworkNames, extraOptionalFrameworkNames);
#endif


            File.WriteAllText(pbxProjectPath, pbxProject.WriteToString());
        }

        private static void _AddFrameWorkToProject
        (
            PBXProject pbxProject,
            string guid,
            string[] extraFrameworkNames,
            string[] extraOptionalFrameworkNames
        )
        {
            if (extraFrameworkNames != null)
            {
                foreach (var frameworkName in extraFrameworkNames)
                {
                    if (frameworkName.EndsWith(".tbd"))
                    {
                        pbxProject.AddFileToBuild(guid, pbxProject.AddFile("usr/lib/" + frameworkName, "Frameworks/" + frameworkName, PBXSourceTree.Sdk));
                    }
                    else
                    {
                        pbxProject.AddFrameworkToProject(guid, frameworkName, false);
                    }
                }
            }

            if (extraOptionalFrameworkNames != null)
            {
                foreach (var frameworkName in extraOptionalFrameworkNames)
                {
                    pbxProject.AddFrameworkToProject(guid, frameworkName, true);
                }
            }
        }


        private static void _SetBuildSettings(string pathToBuiltProject, IDictionary<string,string> settings)
        {
            if( settings ==null || settings.Count == 0)
            {
                return;
            }

            var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxProjectPath);

            var targetGuid = _GetMainTargetGuid(pbxProject);

            foreach( var kvp in settings)
            {
                pbxProject.SetBuildProperty(targetGuid, kvp.Key, kvp.Value);
            }

#if UNITY_2019_3_OR_NEWER
            var frameworkGuid = _GetUnityFrameworkGuid(pbxProject);

            foreach (var kvp in settings)
            {
                pbxProject.SetBuildProperty(frameworkGuid, kvp.Key, kvp.Value);
            }
#endif

            File.WriteAllText(pbxProjectPath, pbxProject.WriteToString());
        }


        private static IDictionary<string, string> _PrepareBuildSettings(XcodePatcherConfig config)
        {
            var ret = new Dictionary<string, string>();
            if (config.IsEnablingObjcExceptions)
            {
                ret.Add(_gccEnableObjcException, "YES");
            }

            if (config.IsDisablingBitcode)
            {
                ret.Add(_enableBitcode, "NO");
            }

            if(config.IsEmbeddingSwiftStandardLibraries)
            {
                ret.Add(_embeddingSwiftStandardLibraries, "YES");
            }

            return ret;
        }


        private static void _SetiCloudEntitlement
        (
            bool isAddingiCloudEntitlement,
            List<string> newSystemCapabilities
        )
        {
            if (!isAddingiCloudEntitlement)
            {
                return;
            }
            
            newSystemCapabilities.Add("com.apple.iCloud");
        }


        private static void _SetPushNotification
        (
            string pathToBuiltProject,
            bool isSettingsPushNotification,
            List<string> newSystemCapabilities
        )
        {
            if (!isSettingsPushNotification)
            {
                return;
            }

            newSystemCapabilities.Add("com.apple.BackgroundModes");
            newSystemCapabilities.Add("com.apple.Push");

            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            var backgroundModeArray = infoPlistRootDict.CreateArray("UIBackgroundModes");
            backgroundModeArray.AddString("remote-notification");

            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }


        private static void _SetToFullScreenOnly(string pathToBuiltProject)
        {
            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            infoPlistRootDict.SetBoolean("UIRequiresFullScreen", true);
            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }


        private static void _SetFacebookAppTransportSecurityWhiteList
        (
            string pathToBuiltProject,
            bool isAddingFacebookAppTransportSecurity,
            bool isWhiteListingFacebookSDKOlderThanV45,
            bool isWhiteListingFacebookSDKOlderThanV46,
            bool isWhiteListingFacebookSDKV46
        )
        {
            if (!isAddingFacebookAppTransportSecurity)
            {
                return;
            }

            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            var NSAppTransportSecurityDict = infoPlistRootDict.CreateDict("NSAppTransportSecurity");
            var NSExceptionDomainsDict = NSAppTransportSecurityDict.CreateDict("NSExceptionDomains");
            var FacebookDict = NSExceptionDomainsDict.CreateDict("facebook.com");
            FacebookDict.SetBoolean("NSIncludesSubdomains", true);
            FacebookDict.SetBoolean("NSThirdPartyExceptionRequiresForwardSecrecy", false);

            var FBCDNkDict = NSExceptionDomainsDict.CreateDict("fbcdn.net");
            FBCDNkDict.SetBoolean("NSIncludesSubdomains", true);
            FBCDNkDict.SetBoolean("NSThirdPartyExceptionRequiresForwardSecrecy", false);

            var AKAMAIHDDict = NSExceptionDomainsDict.CreateDict("akamaihd.net");
            AKAMAIHDDict.SetBoolean("NSIncludesSubdomains", true);
            AKAMAIHDDict.SetBoolean("NSThirdPartyExceptionRequiresForwardSecrecy", false);

            var LSApplicationQueriesSchemesArray = infoPlistRootDict["LSApplicationQueriesSchemes"] as PlistElementArray;
            if (LSApplicationQueriesSchemesArray == null)
            {
                LSApplicationQueriesSchemesArray = infoPlistRootDict.CreateArray("LSApplicationQueriesSchemes");
            }
            if (isWhiteListingFacebookSDKOlderThanV45)
            {
                LSApplicationQueriesSchemesArray.AddString("fbapi");
                LSApplicationQueriesSchemesArray.AddString("fbapi20130214");
                LSApplicationQueriesSchemesArray.AddString("fbapi20130410");
                LSApplicationQueriesSchemesArray.AddString("fbapi20130702");
                LSApplicationQueriesSchemesArray.AddString("fbapi20131010");
                LSApplicationQueriesSchemesArray.AddString("fbapi20131219");
                LSApplicationQueriesSchemesArray.AddString("fbapi20140410");
                LSApplicationQueriesSchemesArray.AddString("fbapi20140116");
                LSApplicationQueriesSchemesArray.AddString("fbapi20150313");
                LSApplicationQueriesSchemesArray.AddString("fbapi20150629");
                LSApplicationQueriesSchemesArray.AddString("fbauth");
                LSApplicationQueriesSchemesArray.AddString("fbauth2");
                LSApplicationQueriesSchemesArray.AddString("fb-messenger-api20140430");
            }

            if (isWhiteListingFacebookSDKOlderThanV46)
            {
                LSApplicationQueriesSchemesArray.AddString("fb-messenger-platform-20150128");
                LSApplicationQueriesSchemesArray.AddString("fb-messenger-platform-20150218");
                LSApplicationQueriesSchemesArray.AddString("fb-messenger-platform-20150305");
            }

            if (isWhiteListingFacebookSDKV46)
            {
                LSApplicationQueriesSchemesArray.AddString("fbapi");
                LSApplicationQueriesSchemesArray.AddString("fb-messenger-api");
                LSApplicationQueriesSchemesArray.AddString("fbauth2");
                LSApplicationQueriesSchemesArray.AddString("fbshareextension");
            }

            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }


        private static void _SetProductBundleIdentifier
        (
            string pathToBuiltProject,
            bool isSettingProductBundleIdentifier,
            string bundleIdentifier
        )
        {
            if (!isSettingProductBundleIdentifier)
            {
                return;
            }

            var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxProjectPath);
            var pbxProjectString = pbxProject.WriteToString();

            var productBundleIdentifier = string.Format(_productBundleIdentifierTemplate, bundleIdentifier);

            pbxProjectString = pbxProjectString.Replace(_productBundleIdentifierAnchor, productBundleIdentifier);

            File.WriteAllText(pbxProjectPath, pbxProjectString);
        }


        private static void _CopyFastlaneFolder
        (
            string pathToBuiltProject,
            bool isCopyingFastlaneFolder,
            string fastlaneFolderPath
        )
        {
            if (!isCopyingFastlaneFolder)
            {
                return;
            }

            var fastlaneSourcePath = Path.Combine(Directory.GetParent(Application.dataPath).ToString(),
                fastlaneFolderPath);
            var fastlaneTargetPath = Path.Combine(pathToBuiltProject, "fastlane");
            FileUtil.CopyFileOrDirectory(fastlaneSourcePath, fastlaneTargetPath);
        }


        private static void _AddBundleURL
        (
            string pathToBuiltProject,
            bool isAddingCFBundleURL,
            string bundleURLName,
            string[] bundleURLSchemes
        )
        {
            if (!isAddingCFBundleURL)
            {
                return;
            }

            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            var bundleURLArray = infoPlistRootDict["CFBundleURLTypes"] as PlistElementArray;
            if (bundleURLArray == null)
            {
                bundleURLArray = infoPlistRootDict.CreateArray("CFBundleURLTypes");
            }
            var bundleURLDict = bundleURLArray.AddDict();

            bundleURLDict.SetString("CFBundleTypeRole", "Editor");
            bundleURLDict.SetString("CFBundleURLName", bundleURLName);
            var bundleURLSchemesArray = bundleURLDict.CreateArray("CFBundleURLSchemes");
            if (bundleURLSchemes != null)
            {
                foreach (var bundleUrlScheme in bundleURLSchemes)
                {
                    bundleURLSchemesArray.AddString(bundleUrlScheme);
                }
            }

            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }

        private static void _AddBundleLocalizations
        (
            string pathToBuiltProject,
            string[] bundleLocalizations
        )
        {
            if (bundleLocalizations == null || bundleLocalizations.Length == 0)
                return;
            
            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            var plistArray = infoPlistRootDict["CFBundleLocalizations"] as PlistElementArray;
            if (plistArray == null)
            {
                plistArray = infoPlistRootDict.CreateArray("CFBundleLocalizations");
            }

            foreach(var item in bundleLocalizations)
            {
                plistArray.AddString(item);
            }

            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }

        private static void _WriteSystemCapabilities
        (
            string pathToBuiltProject,
            string developmentTeamPbxElementId,
            List<string> newSystemCapabilities
        )
        {
            if (newSystemCapabilities.Count == 0) { return; }

            if (string.IsNullOrEmpty(developmentTeamPbxElementId))
            {
                _Log("Missing development team PBX element id");
                return;
            }

            var targetAttributePbxElementId = _GenerateGuid();

            var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxProjectPath);
            var pbxProjectString = pbxProject.WriteToString();

            var capabilities = string.Empty;
            foreach (var capability in newSystemCapabilities)
            {
                capabilities += string.Format(_systemCapabilityTemplate, capability);
            }

            // From Unity 5 Added tvOS controller capability by default,
            // and we need to patch PBX in another way.
            var anchor = string.Format("{0}\n\t\t\t\t\t{1} = {{", _targetAttributeAnchor, targetAttributePbxElementId);
            var teamAttribute = string.Format(_developmentTeamTemplate, developmentTeamPbxElementId);
            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, anchor, teamAttribute);
            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _systemCapabilitiesAnchor, capabilities);

            File.WriteAllText(pbxProjectPath, pbxProjectString);
        }


        private static void _InstallEntitlementsFile
        (
            string pathToBuiltProject,
            bool hasEntitlement,
            bool isCopyingEntitlementFileRequired,
            string entitlementFileName,
            string entitlementSourceFileAssetPath
        )
        {
            if (!hasEntitlement)
            {
                if (isCopyingEntitlementFileRequired)
                {
                    _Log("Error: Using capabilities without providing entitlement file");
                }

                return;
            }

            var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxProjectPath);
            var pbxProjectString = pbxProject.WriteToString();

            var entitlementFilePbxElementId = _GenerateGuid();
            var pbxFileReferenceContent = string.Format(_entitlementPbxFileReferenceTemplate, entitlementFilePbxElementId,
                entitlementFileName);
            var customTemplateContent = string.Format(_customTemplateContentTemplate, entitlementFilePbxElementId,
                entitlementFileName);
            var entitlementContent = string.Format(_entitlementContentTemplate, entitlementFileName);

            var entitlementDestinationRelativePath = string.Format(_entitlementDestinationRelativePathTemplate,
                entitlementFileName);
            var entitlementSourcePath = Path.Combine(Application.dataPath, entitlementSourceFileAssetPath);
            var entitlementDestinationPath = Path.Combine(pathToBuiltProject, entitlementDestinationRelativePath);
            File.Copy(entitlementSourcePath, entitlementDestinationPath);


            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxFileReferenceAnchor, pbxFileReferenceContent);
            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _customTemplateAnchor, customTemplateContent);
            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _xcBuildConfigurationAnchor, entitlementContent);

            File.WriteAllText(pbxProjectPath, pbxProjectString);
        }

        private static void _SetAddingPhotoLibraryDescription
        (
            string pathToBuiltProject,
            bool enabled,
            string description,
            Dictionary<UsageDescriptionLocale, string> descriptionLocalization,
            Dictionary<UsageDescriptionLocale, List<string>> usageDescriptionLocales
        )
        {
            _AddPurposeDescription
            (
                pathToBuiltProject,
                enabled,
                "NSPhotoLibraryUsageDescription",
                description,
                descriptionLocalization,
                usageDescriptionLocales
            );

            _AddPurposeDescription
            (
                pathToBuiltProject,
                enabled,
                "NSPhotoLibraryAddUsageDescription",
                description,
                descriptionLocalization,
                usageDescriptionLocales
            );
        }

        private static void _SetLocationWhenInUseUsageDescription
        (
            string pathToBuiltProject,
            bool enabled,
            string description,
            Dictionary<UsageDescriptionLocale, string> descriptionLocalization,
            Dictionary<UsageDescriptionLocale, List<string>> usageDescriptionLocales
        ) => _AddPurposeDescription
        (
            pathToBuiltProject,
            enabled,
            "NSLocationWhenInUseUsageDescription",
            description,
            descriptionLocalization,
            usageDescriptionLocales
        );

        private static void _SetUserTrackingUsageDescription
        (
            string pathToBuiltProject,
            bool enabled,
            string description,
            Dictionary<UsageDescriptionLocale, string> descriptionLocalization,
            Dictionary<UsageDescriptionLocale, List<string>> usageDescriptionLocales
        ) => _AddPurposeDescription
        (
            pathToBuiltProject,
            enabled,
            "NSUserTrackingUsageDescription",
            description,
            descriptionLocalization,
            usageDescriptionLocales
        );

        private static void _AddPurposeDescription
        (
            string pathToBuiltProject,
            bool enabled,
            string purposeName,
            string description,
            Dictionary<UsageDescriptionLocale, string> descriptionLocalization,
            Dictionary<UsageDescriptionLocale, List<string>> usageDescriptionLocales
        )
        {
            if (!enabled)
                return;

            if (description == null)
                description = string.Empty;

            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            infoPlist.root.SetString(purposeName, description);
            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());

            if (descriptionLocalization == null)
                return;

            foreach (var localization in descriptionLocalization)
            {
                if (!usageDescriptionLocales.ContainsKey(localization.Key))
                {
                    usageDescriptionLocales.Add(localization.Key, new List<string>());
                }

                usageDescriptionLocales[localization.Key].Add($"\"{purposeName}\" = \"{localization.Value}\";");
            }
        }

        // https://developer.apple.com/forums/thread/652801
        // https://developer.apple.com/library/archive/documentation/General/Reference/InfoPlistKeyReference/Articles/AboutInformationPropertyListFiles.html
        private static void _SetUsageDescriptionLocalization
        (
            string pathToBuiltProject,
            Dictionary<UsageDescriptionLocale, List<string>> usageDescriptionLocales
        )
        {
            if (usageDescriptionLocales.Count == 0)
                return;

            var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
            var pbxProject = new PBXProject();
            pbxProject.ReadFromFile(pbxProjectPath);
            var pbxProjectString = pbxProject.WriteToString();

            var pbxBuildFileId = _GenerateGuid();
            var pbxFileReferenceId = _GenerateGuid();

            var pbxBuildFileReference = string.Format(_pbxBuildFileReferenceTemplate, pbxBuildFileId, "InfoPlist.strings", pbxFileReferenceId);
            var customTemplateContent = string.Format(_customTemplateContentTemplate, pbxFileReferenceId, "InfoPlist.strings");
            var pbxResourceBuildPhase = string.Format(_pbxResourcesBuildPhaseTemplate, pbxBuildFileId, "InfoPlist.strings"); 

            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxBuildFileAnchor, pbxBuildFileReference);
            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _customTemplateAnchor, customTemplateContent);
            pbxProjectString = StringUtility.InsertAfterMatch(pbxProjectString, _pbxResourcesBuildPhaseRegex, pbxResourceBuildPhase);

            var localeChildrenBuilder = new StringBuilder();
            foreach (var locale in usageDescriptionLocales)
            {
                var localeFileId = _GenerateGuid();
                var localeString = _GetLocaleString(locale.Key);
                var infoPlistStringPath = $"{_GetLocaleString(locale.Key)}.lproj/InfoPlist.strings";
                var pbxFileReferenceContent = string.Format(_textPbxFileReferenceTemplate, localeFileId, localeString, infoPlistStringPath);
               
                pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxFileReferenceAnchor, pbxFileReferenceContent);

                var infoPlistStringFullPath = Path.Combine(pathToBuiltProject, infoPlistStringPath);
                var infoPlistStringFolder = Path.GetDirectoryName(infoPlistStringFullPath);
                if (!Directory.Exists(infoPlistStringFolder))
                {
                    Directory.CreateDirectory(infoPlistStringFolder);
                }
                File.WriteAllText(infoPlistStringFullPath, String.Join(Environment.NewLine, locale.Value));

                localeChildrenBuilder.AppendLine($"{localeFileId} /* {localeString} */,");
            }

            var pbxVariantGroup = string.Format(_pbxVariantGroupTemplate, pbxFileReferenceId, "InfoPlist.strings", localeChildrenBuilder.ToString());
            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxVariantGroupAnchor, pbxVariantGroup);


            File.WriteAllText(pbxProjectPath, pbxProjectString);
        }

        private static string _GetLocaleString(UsageDescriptionLocale locale)
        {
            switch (locale)
            {
                case UsageDescriptionLocale.English:            return "en";
                case UsageDescriptionLocale.Chinese:            return "zh";
                case UsageDescriptionLocale.ChineseTraditional: return "zh_TW";
                case UsageDescriptionLocale.ChineseSimplified:  return "zh_CN";
                case UsageDescriptionLocale.Japanese:           return "ja";
                case UsageDescriptionLocale.Korean:             return "ko";
                case UsageDescriptionLocale.Vietnamese:         return "vi";
                case UsageDescriptionLocale.Thai:               return "th";
                case UsageDescriptionLocale.Portuguese:         return "pt";
                case UsageDescriptionLocale.Spanish:            return "es";
                case UsageDescriptionLocale.German:             return "de";
                case UsageDescriptionLocale.Russian:            return "ru";
                case UsageDescriptionLocale.French:             return "fr";
                case UsageDescriptionLocale.Arabic:             return "ar";
                default:                                        return "en";
            }
        }



        private static void _SetIsAllowMixedLocalizations
        (
            string pathToBuiltProject,
            bool isAllowMixedLocalizations
        )
        {
            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            infoPlistRootDict.SetBoolean("CFBundleAllowMixedLocalizations", isAllowMixedLocalizations);
            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }

        private static void _SetAppUsesNonExemptEncryption
        (
            string pathToBuiltProject,
            bool isUsingNonExemptEncryption
        )
        {
            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            infoPlistRootDict.SetBoolean("ITSAppUsesNonExemptEncryption", isUsingNonExemptEncryption);
            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }

        private static void _SetRunWithAddressSanitizer
        (
            string pathToBuiltProject,
            bool isRunWithAddressSanitizer
        )
        {
            if (!isRunWithAddressSanitizer)
            {
                return;
            }

            string schemePath = _GetSchemePath(pathToBuiltProject);
            var schemeDocument = XDocument.Load(schemePath);

            var launchElement = schemeDocument.Root.XPathSelectElement("./LaunchAction");
            launchElement.Add(new XAttribute("enableAddressSanitizer", "YES"));
            File.WriteAllText(schemePath, schemeDocument.Root.ToString(), new UTF8Encoding(false));
        }

        private static void _SetBundleName
        (
            string pathToBuiltProject,
            bool isSettingCFBundleName,
            string cfBundleName
        )
        {
            if (!isSettingCFBundleName)
            {
                return;
            }

            var infoPlistPath = _GetInfoPlistPath(pathToBuiltProject);
            var infoPlist = new PlistDocument();
            infoPlist.ReadFromString(File.ReadAllText(infoPlistPath));
            var infoPlistRootDict = infoPlist.root;

            infoPlistRootDict.SetString("CFBundleName", cfBundleName);

            File.WriteAllText(infoPlistPath, infoPlist.WriteToString());
        }

        private static void _ReplaceFiles
        (
            string pathToBuiltProject,
            IEnumerable<ReplaceFilePathData> replaceFilePaths
        )
        {
            if( replaceFilePaths == null){
                return;
            }

            foreach(var replaceFilePath in replaceFilePaths){
                var sourcePath = replaceFilePath.SourceAbsolutePath;
                var targetPath = Path.Combine(pathToBuiltProject, replaceFilePath.DestinationRelativePath);

                FileUtil.ReplaceFile( sourcePath, targetPath );
            }
        }

        private static void _AddPlistFileToReferences
        (
            string pathToBuiltProject,
            IEnumerable<AddFileReferenceData> addToPbxProjectPlistFiles
        )
        {
            if (addToPbxProjectPlistFiles == null)
            {
                return;
            }

            foreach (var addToPbxProjectFile in addToPbxProjectPlistFiles)
            {
                var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
                var pbxProject = new PBXProject();
                pbxProject.ReadFromFile(pbxProjectPath);
                var pbxProjectString = pbxProject.WriteToString();

                var pbxBuildFileId = _GenerateGuid();
                var pbxFileReferenceId = _GenerateGuid();

                var pbxBuildFileReference = string.Format(_pbxBuildFileReferenceTemplate, pbxBuildFileId, addToPbxProjectFile.DestinationFilename, pbxFileReferenceId);
                var pbxFileReferenceContent = string.Format(_plistPbxFileReferenceTemplate, pbxFileReferenceId, addToPbxProjectFile.DestinationFilename);
                var customTemplateContent = string.Format(_customTemplateContentTemplate, pbxFileReferenceId, addToPbxProjectFile.DestinationFilename);
                var pbxResourceBuildPhase = string.Format(_pbxResourcesBuildPhaseTemplate, pbxBuildFileId, addToPbxProjectFile.DestinationFilename);

                pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxBuildFileAnchor, pbxBuildFileReference);
                pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxFileReferenceAnchor, pbxFileReferenceContent);
                pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _customTemplateAnchor, customTemplateContent);
                pbxProjectString = StringUtility.InsertAfterMatch(pbxProjectString, _pbxResourcesBuildPhaseRegex, pbxResourceBuildPhase);

                File.WriteAllText(pbxProjectPath, pbxProjectString);

                File.Copy(addToPbxProjectFile.SourceFilePath, Path.Combine(pathToBuiltProject, addToPbxProjectFile.DestinationFilename));
            }
        }

        private static void _AddSwiftFileToReferences
        (
            string pathToBuiltProject,
            IEnumerable<AddFileReferenceData> addToPbxProjectSwiftFiles
        )
        {
            if (addToPbxProjectSwiftFiles == null)
            {
                return;
            }

            foreach (var addToPbxProjectFile in addToPbxProjectSwiftFiles)
            {
                var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
                var pbxProject = new PBXProject();
                pbxProject.ReadFromFile(pbxProjectPath);
                var pbxProjectString = pbxProject.WriteToString();

                var pbxBuildFileId = _GenerateGuid();
                var pbxFileReferenceId = _GenerateGuid();

                var pbxBuildFileReference = string.Format(_pbxBuildFileReferenceTemplate, pbxBuildFileId, addToPbxProjectFile.DestinationFilename, pbxFileReferenceId);
                var pbxFileReferenceContent = string.Format(_swiftPbxFileReferenceTemplate, pbxFileReferenceId, addToPbxProjectFile.DestinationFilename);
                var customTemplateContent = string.Format(_customTemplateContentTemplate, pbxFileReferenceId, addToPbxProjectFile.DestinationFilename);
                var pbxSourceBuildPhase = string.Format(_pbxSourcesBuildPhaseTemplate, pbxBuildFileId, addToPbxProjectFile.DestinationFilename);

                pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxBuildFileAnchor, pbxBuildFileReference);
                pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxFileReferenceAnchor, pbxFileReferenceContent);
                pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _customTemplateAnchor, customTemplateContent);
                pbxProjectString = StringUtility.InsertAfterMatch(pbxProjectString, _pbxSourcesBuildPhaseRegex, pbxSourceBuildPhase);

                File.WriteAllText(pbxProjectPath, pbxProjectString);

                File.Copy(addToPbxProjectFile.SourceFilePath, Path.Combine(pathToBuiltProject, addToPbxProjectFile.DestinationFilename));
            }
        }

        private static void _AddSwiftBridgingHeader
        (
            string pathToBuiltProject,
            bool isAddingSwiftBridgingHeader,
            string swiftBridgingHeaderSourceFilePath,
            string swiftBridgingHeaderDestinationFilename
        )
        {
            if (!isAddingSwiftBridgingHeader)
            {
                return;
            }

            var pbxProject = new PBXProject();
            var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
            pbxProject.ReadFromFile(pbxProjectPath);
            var pbxProjectString = pbxProject.WriteToString();

            var swiftBridgingHeaderPbxFileReferenceId = _GenerateGuid();
            string pbxFileReferenceContent = string.Format(_cHeaderPbxFileReferenceTemplate, swiftBridgingHeaderPbxFileReferenceId, swiftBridgingHeaderDestinationFilename);
            var customTemplateContent = string.Format(_customTemplateContentTemplate, swiftBridgingHeaderPbxFileReferenceId, swiftBridgingHeaderDestinationFilename);

            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _pbxFileReferenceAnchor, pbxFileReferenceContent);
            pbxProjectString = StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _customTemplateAnchor, customTemplateContent);

            pbxProjectString = _SetXcodeBuildConfigurationAttribute(pbxProjectString, "SWIFT_OBJC_BRIDGING_HEADER", swiftBridgingHeaderDestinationFilename);
         
            File.WriteAllText(pbxProjectPath, pbxProjectString);

            File.Copy(swiftBridgingHeaderSourceFilePath, Path.Combine(pathToBuiltProject, swiftBridgingHeaderDestinationFilename));
        }

        private static void _SetXcodeBuildConfiguration
        (
            string pathToBuiltProject,
            Dictionary<string, string> attributes
        )
        {
            var pbxProject = new PBXProject();
            var pbxProjectPath = _GetPBXProjectPath(pathToBuiltProject);
            pbxProject.ReadFromFile(pbxProjectPath);
            var pbxProjectString = pbxProject.WriteToString();

            foreach (var attribute in attributes)
            {
                pbxProjectString = _SetXcodeBuildConfigurationAttribute(pbxProjectString, attribute.Key, attribute.Value);
            }

            File.WriteAllText(pbxProjectPath, pbxProjectString);
        }

        private static string _SetXcodeBuildConfigurationAttribute(string pbxProjectString, string attributeName, string attributeValue)
        {
            var xcBuildConfigurationAttributeRegex = string.Format(_xcBuildConfigurationAttributeRegexTemplate, attributeName);
            var _xcBuildConfigurationAttribute = string.Format(_xcBuildConfigurationAttributeTemplate, attributeName, attributeValue);
            if (Regex.IsMatch(pbxProjectString, xcBuildConfigurationAttributeRegex))
            {
                return Regex.Replace(pbxProjectString, xcBuildConfigurationAttributeRegex, _xcBuildConfigurationAttribute);
            }
            else
            {
                return StringUtility.InsertAllAfterWithNewLine(pbxProjectString, _xcBuildConfigurationAnchor, _xcBuildConfigurationAttribute);
            }
        }

#region Utility
        private static string _GetMainTargetGuid(PBXProject pbxProject)
#if UNITY_2019_3_OR_NEWER
            => pbxProject.GetUnityMainTargetGuid();
#else
            => pbxProject.TargetGuidByName("Unity-iPhone");
#endif

#if UNITY_2019_3_OR_NEWER
        private static string _GetUnityFrameworkGuid(PBXProject pbxProject)
            => pbxProject.TargetGuidByName("UnityFramework");
#endif

        private static string _GenerateGuid()
            => Guid.NewGuid().ToString("N").Substring(8).ToUpper();

        private static string _GetInfoPlistPath(string pathToBuiltProject)
            => Path.Combine(pathToBuiltProject, "./Info.plist");

        private static string _GetPBXProjectPath(string pathToBuiltProject)
            => Path.Combine(pathToBuiltProject, "./Unity-iPhone.xcodeproj/project.pbxproj");

        private static string _GetSchemePath(string pathToBuiltProject)
            => Path.Combine(pathToBuiltProject, "./Unity-iPhone.xcodeproj/xcshareddata/xcschemes/Unity-iPhone.xcscheme");

        private static void _Log(string message)
        {
            if (Application.isEditor)
            {
                Debug.Log("[XcodePostprocessor]: " + message);
            }
            else if (InternalEditorUtility.inBatchMode)
            {
                Console.WriteLine("[XcodePostprocessor]: " + message);
            }
        }
#endregion
    }
}
#endif