using NUnit.Framework;

namespace Rayark.PatchwerkLite.Editor.Tests
{
    [TestFixture]
    public class StringUtilityTests
    {
        [Test]
        public void InsertAllAfter_Found_Inserted()
        {
            const string insertTarget = "aaabbbccc";
            const string searchTarget = "bbb";
            const string insertString = "ddd";

            string result = StringUtility.InsertAllAfter(insertTarget, searchTarget, insertString);

            Assert.AreEqual("aaabbbdddccc", result);
        }

        [Test]
        public void InsertAllAfter_NotFound_NoInsert()
        {
            const string insertTarget = "aaabbccc";
            const string searchTarget = "bbb";
            const string insertString = "ddd";

            string result = StringUtility.InsertAllAfter(insertTarget, searchTarget, insertString);

            Assert.AreEqual(insertTarget, result);
        }

        [Test]
        public void InsertAllAfterWithNewLine_Found_InsertWithNewLine()
        {
            const string insertTarget = "aaabbbccc";
            const string searchTarget = "bbb";
            const string insertString = "ddd";

            string result = StringUtility.InsertAllAfterWithNewLine(insertTarget, searchTarget, insertString);

            Assert.AreEqual("aaabbb\ndddccc", result);
        }

        [Test]
        public void InsertAllAfterWithNewLine_NotFound_NoInsert()
        {
            const string insertTarget = "aaabbccc";
            const string searchTarget = "bbb";
            const string insertString = "ddd";

            string result = StringUtility.InsertAllAfter(insertTarget, searchTarget, insertString);

            Assert.AreEqual(insertTarget, result);
        }

        [Test]
        public void InsertAllAfter_MultipleFound_InsertAll()
        {
            const string insertTarget = "bbbaaabbbcccbbb";
            const string searchTarget = "bbb";
            const string insertString = "ddd";

            string result = StringUtility.InsertAllAfter(insertTarget, searchTarget, insertString);

            Assert.AreEqual("bbbdddaaabbbdddcccbbbddd", result);
        }

        [Test]
        public void InsertAfterMatch_Found_Insert()
        {
            const string insertTarget =
@"/* Begin PBXResourcesBuildPhase section */
		1D60588D0D05DD3D006BFB54 /* Resources */ = {
			isa = PBXResourcesBuildPhase;
			buildActionMask = 2147483647;
			files = (
";
            const string searchRegex =
                @"\/\* Begin PBXResourcesBuildPhase section \*\/\n\t*[A-F0-9]{24} \/\* Resources \*\/ = {\n\t*isa = PBXResourcesBuildPhase;\n\t*buildActionMask = [0-9]*;\n\t*files = \(\n";
            const string insertString = "\t\t\t64E14E2780C69C187093F625 /* GoogleService-Info.plist in Resources */,";

            string result = StringUtility.InsertAfterMatch(insertTarget, searchRegex, insertString);

            Assert.AreEqual(
@"/* Begin PBXResourcesBuildPhase section */
		1D60588D0D05DD3D006BFB54 /* Resources */ = {
			isa = PBXResourcesBuildPhase;
			buildActionMask = 2147483647;
			files = (
			64E14E2780C69C187093F625 /* GoogleService-Info.plist in Resources */,", result);
        }

        [Test]
        public void InsertAfterMatch_NotFound_ReturnNull()
        {
            const string insertTarget =
@"/* Begin PBXSourcesBuildPhase section */
		1D60588E0D05DD3D006BFB54 /* Sources */ = {
			isa = PBXSourcesBuildPhase;
			buildActionMask = 2147483647;
			files = (
";
            const string searchRegex =
                @"\/\* Begin PBXResourcesBuildPhase section \*\/\n\t*[A-F0-9]{24} \/\* Resources \*\/ = {\n\t*isa = PBXResourcesBuildPhase;\n\t*buildActionMask = [0-9]*;\n\t*files = \(\n";
            const string insertString = "\t\t\t64E14E2780C69C187093F625 /* GoogleService-Info.plist in Resources */,";

            string result = StringUtility.InsertAfterMatch(insertTarget, searchRegex, insertString);

            Assert.AreEqual(null, result);
        }
    }
}