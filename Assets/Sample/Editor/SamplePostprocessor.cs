#if UNITY_IOS
using System.IO;
using System.Collections.Generic;

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;

namespace Rayark.Sample
{
    using Rayark.PatchwerkLite;

    public static class SamplePostprocessor
    {
        [PostProcessBuild(1000)]
        public static void _OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
        {
            if (target != BuildTarget.iOS)
            {
                return;
            }

            var config = new XcodePatcherConfig();

            config.AddExtraDeviceCapabilities(new[] { "opengles-3" });
            config.AddExtraFrameworks(new[] { "CoreData.framework" }, new[] { "AuthenticationServices.framework" });

            config.IsAddingiCloudEntitlement = true;
            config.IsAddingPushNotificationEntitlement = true;
            config.SetSystemCapabilitiesDevelopmentTeamId("4UBYM7B699");
            config.SetEntitlement(Path.Combine(Application.dataPath, "Sample/patchwerk-lite.entitlements"));

            config.IsDisablingBitcode = true;
            config.IsEnablingObjcExceptions = true;
            config.IsEmbeddingSwiftStandardLibraries = true;
            config.SetFacebookAppTransportSecurity(true, true, true);
            config.IsSettingProductBundleIdentifier = true;
            config.SetFastlaneFolderPath(
                Path.Combine(Application.dataPath, "Sample/fastlane/"));

            config.SetCFBundleURL("com.rayark.patchwerk-lite", new[] { "patchwerk-lite" });
            config.SetCFBundleLocalizations(new[] { "zh", "en", "ja", "ko", "zh_TW", "zh_CN" });

            config.SetPhotoLibraryUsageDescription("AccessPhoto", new Dictionary<UsageDescriptionLocale, string>
            {
                [UsageDescriptionLocale.English] = "Access Photo",
                [UsageDescriptionLocale.Chinese] = "訪問照片",
                [UsageDescriptionLocale.ChineseTraditional] = "存取照片",
                [UsageDescriptionLocale.ChineseSimplified] = "访问照片",
                [UsageDescriptionLocale.Japanese] = "写真にアクセス",
                [UsageDescriptionLocale.Korean] = "액세스 사진",
                [UsageDescriptionLocale.Vietnamese] = "Truy cập ảnh",
                [UsageDescriptionLocale.Thai] = "เข้าถึงรูปภาพ",
                [UsageDescriptionLocale.Portuguese] = "Acesse a foto",
                [UsageDescriptionLocale.Spanish] = "Foto de acceso",
                [UsageDescriptionLocale.German] = "Zugriff auf Foto",
                [UsageDescriptionLocale.Russian] = "Доступ к фото",
                [UsageDescriptionLocale.French] = "Accéder à la photo",
                [UsageDescriptionLocale.Arabic] = "الوصول إلى الصورة",
            });
            config.SetAddingLocationWhenInUseUsageDescription("AccessLocation", new Dictionary<UsageDescriptionLocale, string>
            {
                [UsageDescriptionLocale.English] = "Access Location",
                [UsageDescriptionLocale.Chinese] = "訪問位置",
                [UsageDescriptionLocale.ChineseTraditional] = "存取位置",
                [UsageDescriptionLocale.ChineseSimplified] = "访问位置",
                [UsageDescriptionLocale.Japanese] = "アクセス場所",
                [UsageDescriptionLocale.Korean] = "액세스 위치",
                [UsageDescriptionLocale.Vietnamese] = "Truy cập vị trí",
                [UsageDescriptionLocale.Thai] = "เข้าถึงตำแหน่ง",
                [UsageDescriptionLocale.Portuguese] = "Localização de acesso",
                [UsageDescriptionLocale.Spanish] = "Ubicación de acceso",
                [UsageDescriptionLocale.German] = "Zugangsort",
                [UsageDescriptionLocale.Russian] = "Доступ к местоположению",
                [UsageDescriptionLocale.French] = "Emplacement d'accès",
                [UsageDescriptionLocale.Arabic] = "الالوصول إلى الموقع",
            });
            config.SetAddingUserTrackingUsageDescription("AccessTracking", new Dictionary<UsageDescriptionLocale, string>
            {
                [UsageDescriptionLocale.English] = "Access Tracking",
                [UsageDescriptionLocale.Chinese] = "訪問跟踪",
                [UsageDescriptionLocale.ChineseTraditional] = "存取追蹤",
                [UsageDescriptionLocale.ChineseSimplified] = "访问跟踪",
                [UsageDescriptionLocale.Japanese] = "アクセス追跡",
                [UsageDescriptionLocale.Korean] = "액세스 추적",
                [UsageDescriptionLocale.Vietnamese] = "Theo dõi truy cập",
                [UsageDescriptionLocale.Thai] = "การติดตามการเข้าถึง",
                [UsageDescriptionLocale.Portuguese] = "Rastreamento de acesso",
                [UsageDescriptionLocale.Spanish] = "Seguimiento de acceso",
                [UsageDescriptionLocale.German] = "Zugriffsverfolgung",
                [UsageDescriptionLocale.Russian] = "Отслеживание доступа",
                [UsageDescriptionLocale.French] = "Suivi des accès",
                [UsageDescriptionLocale.Arabic] = "التتبع الوصول",
            });

            config.IsAllowMixedLocalizations = true;
            config.IsUsingNonExemptEncryption = false;
            config.IsRunWithAddressSanitizer = true;
            config.SetCFBundleName("Patchwerk Lite");

            config.AddPlistFileToPbxProject(
                Path.Combine(Application.dataPath, "../GoogleSignIn/GoogleService-Info.plist"),
                "GoogleService-Info.plist");

            config.SetXcodeBuildConfiguration("SWIFT_VERSION", "5.0");
            config.SetXcodeBuildConfiguration("SWIFT_OPTIMIZATION_LEVEL", "-Onone");

            XcodePatcher.Patch(pathToBuiltProject, config);
        }
    }
}
#endif