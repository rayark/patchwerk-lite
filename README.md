# Patchwerk Lite

Unity Xcode project patcher

## Setup

Extract source code to your Unity Project

## Usage Sample

It's recommended to call Patchwerk Lite from static function tagged with [PostProcessBuildAttribute](https://docs.unity3d.com/ScriptReference/Callbacks.PostProcessBuildAttribute.html) so that it will automatically execute after Xcode project is built by Unity. Create `XcodePatcherConfig` and configure it via public interface, then patch Xcode project with `XcodePatcher.Patch`

```c#
public static class SamplePostprocessor
{
    [PostProcessBuild(1000)]
    public static void _OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {
        if (target != BuildTarget.iOS)
        {
            return;
        }

        var config = new XcodePatcherConfig();

        config.AddExtraDeviceCapabilities(new[] { "opengles-3" });
        config.AddExtraFrameworks(new[] { "CoreData.framework" }, new[] { "AuthenticationServices.framework" });

        config.IsAddingiCloudEntitlement = true;
        config.IsAddingPushNotificationEntitlement = true;
        config.SetSystemCapabilitiesDevelopmentTeamId("4UBYM7B699");
        config.SetEntitlement(Path.Combine(Application.dataPath, "Sample/patchwerk-lite.entitlements"));

        config.IsDisablingBitcode = true;
        config.IsEnablingObjcExceptions = true;
        config.IsEmbeddingSwiftStandardLibraries = true;
        config.SetFacebookAppTransportSecurity(true, true, true);
        config.IsSettingProductBundleIdentifier = true;
        config.SetFastlaneFolderPath(Path.Combine(Application.dataPath, "Sample/fastlane/"));

        config.SetCFBundleURL("com.rayark.patchwerk-lite", new[] { "patchwerk-lite" });
        config.SetCFBundleLocalizations(new[] { "zh", "en", "ja", "ko", "zh_TW", "zh_CN" });

        config.SetPhotoLibraryUsageDescription("AccessPhoto", new Dictionary<UsageDescriptionLocale, string>
        {
            [UsageDescriptionLocale.English] = "Access Photo",
            [UsageDescriptionLocale.Chinese] = "訪問照片",
            [UsageDescriptionLocale.ChineseTraditional] = "存取照片",
            [UsageDescriptionLocale.ChineseSimplified] = "访问照片",
            [UsageDescriptionLocale.Japanese] = "写真にアクセス",
            [UsageDescriptionLocale.Korean] = "액세스 사진",
            [UsageDescriptionLocale.Vietnamese] = "Truy cập ảnh",
            [UsageDescriptionLocale.Thai] = "เข้าถึงรูปภาพ",
            [UsageDescriptionLocale.Portuguese] = "Acesse a foto",
            [UsageDescriptionLocale.Spanish] = "Foto de acceso",
            [UsageDescriptionLocale.German] = "Zugriff auf Foto",
            [UsageDescriptionLocale.Russian] = "Доступ к фото",
            [UsageDescriptionLocale.French] = "Accéder à la photo",
            [UsageDescriptionLocale.Arabic] = "الوصول إلى الصورة",
        });

        config.IsAllowMixedLocalizations = true;
        config.IsUsingNonExemptEncryption = false;
        config.IsRunWithAddressSanitizer = true;
        config.SetCFBundleName("Patchwerk Lite");

        config.AddPlistFileToPbxProject(
            Path.Combine(Application.dataPath, "../GoogleSignIn/GoogleService-Info.plist"),
            "GoogleService-Info.plist");

        config.SetXcodeBuildConfiguration("SWIFT_VERSION", "5.0");
        config.SetXcodeBuildConfiguration("SWIFT_OPTIMIZATION_LEVEL", "-Onone");

        XcodePatcher.Patch(pathToBuiltProject, config);
    }
}
```

## Operations

- AddExtraDeviceCapabilities  
  Add extra device capability requirement to Info.plist (UIRequiredDeviceCapabilities)  
  Capabilities list: https://developer.apple.com/support/required-device-capabilities/  
  It will always include basic capability `armv7`  
  You do not need to include `armv7`

- AddExtraFrameworks  
  Add extra framework reference (required/optional) to PBX project  
  TwitterKit requires `CoreData.framework`  
  Sign in with Apple requires `AuthenticationServices.framework`  
  Facebook later than 8.0.0 requires `MobileCoreServices.framework`

- IsAddingiCloudEntitlement  
  Is adding `com.apple.iCloud` to SystemCapabilities under TargetAttributes in PBX project  
  You need to set up entitlement file as well

- IsAddingPushNotificationEntitlement  
  Is adding `com.apple.BackgroundModes` and `com.apple.Push` to SystemCapabilities in PBX project  
  Also add `remote-notification` to `UIBackgroundModes` in Info.plist  
  You need to set up entitlement file as well

- SetSystemCapabilitiesDevelopmentTeamId  
  If you set anything related to SystemCapabilities, such as iCloud and push notification  
  You need to supply ID of DevelopmentTeam  
  It's 10 characters string which can be found on distribution certificate in Keychain  
  See: https://stackoverflow.com/a/47732584

- SetEntitlement  
  If you set anything requires entitlement file, such as iCloud, push notification or Sign in with Apple  
  You need to supply entitlement file  
  You can get the entitlement file by turning on entitlements in Xcode GUI  
  Copy `Xcode folder/Unity-iPhone/bundle name.entitlements` back to your Unity repo

- IsDisablingBitcode  
  Turn off Bitcode by setting `ENABLE_BITCODE` to `NO` in PBX project BuildProperty  
  You need to turn off Bitcode if any of your iOS plugins does not support Bitcode

- IsEnablingObjcExceptions  
  Turn on Objective-C exception handling by setting `GCC_ENABLE_OBJC_EXCEPTIONS` to `YES` in PBX project BuildProperty  
  You need to turn on Objective-C exception if any of your iOS plugins throws Objective-C exception

- IsEmbeddingSwiftStandardLibraries  
  Embed standard Swift libraries by setting `ALWAYS_EMBED_SWIFT_STANDARD_LIBRARIES` to `YES` in PBX project BuildProperty  
  You need to embed Swift libraries if you have Swift dependencies and need to support iOS 11 and older OS  
  This is required to run Facebook SDK 7.21.2 and later on iOS 11 and earlier

- SetFacebookAppTransportSecurity  
  Add AppTransportSecurity for Facebook SDK  
  See: https://developers.facebook.com/docs/ios/ios9/

- IsSettingProductBundleIdentifier  
  Set BundleIdentifier to App ID  
  You need to set BundleIdentifier if you want to use fastlane on Xcode project

- SetFastlaneFolderPath  
  Copy fastlane settings folder to Xcode project

- SetCFBundleURL  
  Set URL scheme

- SetCFBundleLocalizations  
  Declare supported localization  
  See: https://developer.apple.com/documentation/bundleresources/information_property_list/cfbundlelocalizations

- SetPhotoLibraryUsageDescription   
  Set string to `NSPhotoLibraryUsageDescription` and `NSPhotoLibraryAddUsageDescription`  
  This string will be displayed when you try to access photo library for user like saving screenshots  
  If you access photo library without setting this string, it will trigger `ITMS-90683: Missing Purpose String in Info.plist` warning after uploading a build  
  You can supply localizations for different languages

- SetAddingLocationWhenInUseUsageDescription  
  Set string to `NSLocationWhenInUseUsageDescription`  
  This string will be displayed when you try to access user's location  
  If you access user's location without setting this string, it will trigger `ITMS-90683: Missing Purpose String in Info.plist` warning after uploading a build  
  You can supply localizations for different languages

- SetAddingUserTrackingUsageDescription   
  Set string to `NSUserTrackingUsageDescription`  
  This string will be displayed when you try to prompt tracking option with App Tracking Transparency framework  
  You can supply localizations for different languages

- IsAllowMixedLocalizations   
  Set `CFBundleAllowMixedLocalizations` in Info.plist  
  If any UIKit popup shows in wrong localization, turn this option on

- IsUsingNonExemptEncryption  
  Set `ITSAppUsesNonExemptEncryption` in Info.plist  
  You don't need to answer encryption question on iTunes Connect if you added this field

- IsRunWithAddressSanitizer  
  Set Runtime Sanitization mode

- SetCFBundleName  
  Set `CFBundleName` in Info.plist  
  This bundle name will show up when you try to login with 3rd party service

- AddReplaceFilePath  
  Replace file from source absolute path to destination path relative to built Xcode project 

- AddPlistFileToPbxProject  
  Copy Plist file to built project folder and add file into Xcode project reference

- AddSwiftFileToPbxProject  
  Copy Swift source file to built project folder and add file into Xcode project reference

- AddSwiftBridgingHeader  
  Copy and setup Swift bridging C header file `Unity-iPhone-Bridging-Header.h`

- SetXcodeBuildConfiguration  
  Add or replace Xcode build configuration with given value
